library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SW_read is
	Port ( 
			Clock		 		: in   	STD_LOGIC;
			Clock_o	 		: out   	STD_LOGIC;
			Input_data 		: in   	STD_LOGIC;
         Parallel_load  : buffer  STD_LOGIC := '0';
			led				: buffer  STD_LOGIC := '0';
			SW_data			: out  	STD_LOGIC_VECTOR (15 downto 0):= (others => '0'));
end SW_read;

architecture Behavioral of SW_read is
	signal counter: integer range 0 to 16 := 0;
	signal n: integer range 0 to 1000 := 0;
	signal clk_o: std_logic ;
	begin
	--f10khz:entity work.freq_div generic map(2400) port map(Clock, clk_o);
	Clock_o<=clk_o;
	process(clk_o)
		variable tmp: integer range 0 to 16 :=0;
		begin
	   if rising_edge(clk_o) then
			if tmp = 0 AND Parallel_load = '0' then
				Parallel_load<='1';
			elsif Parallel_load = '1' OR tmp>0 then
				Parallel_load<='0';
				if(tmp = 16) then
					tmp:=0;
					n<=n+1;
					if n = 1000 then
						led<=not led;
						n<=0;
					end if;
				else
					SW_data(15 - tmp)<= Input_data;
					tmp:=tmp+1;
				end if;
				counter<=tmp;
			end if;
		end if;
		end process;
	end Behavioral;