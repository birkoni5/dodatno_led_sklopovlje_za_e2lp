library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity All_in_one is
	Port ( 
			Clock		 		: in	   	STD_LOGIC;
			Reset		 		: in	   	STD_LOGIC;
			-- Switches
			CLK				: out			STD_LOGIC := '0';
			Input_data 		: in   		STD_LOGIC;
         Parallel_load  : buffer  	STD_LOGIC := '0';
			-- Matrix
         Columns 			: out    	STD_LOGIC_VECTOR (2 downto 0);
         Rows 				: out    	STD_LOGIC_VECTOR (2 downto 0);
         Enable 			: out	   	STD_LOGIC := '1';
			-- RGB 
			RGB1			 	: out			STD_LOGIC_VECTOR (2 downto 0);
			RGB2			 	: out			STD_LOGIC_VECTOR (2 downto 0);
			RGB3			 	: out			STD_LOGIC_VECTOR (2 downto 0));
end All_in_one;

architecture Behavioral of All_in_one is
	signal clk_10kHz: 	STD_LOGIC;
	signal clk_1Hz: 		STD_LOGIC;
	signal clk_0Hz1: 		STD_LOGIC;
	signal SW_data:		STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
	signal LEDs:      	STD_LOGIC_VECTOR (0 to 63) :=(others => '0');
	signal Number:  		integer range 0 to 9999;
	begin
	-- switches
	f10khz: entity work.freq_div generic map(2400) port map(Clock, clk_10kHz, Reset);
	CLK <= clk_10kHz;
	read_states_of_switches: entity work.SW_read port map(clk_10kHz, Input_data, Parallel_load, SW_data, Reset);
	-- matrix
	f1hz: entity work.freq_div generic map(24_000_000) port map(Clock, clk_1Hz, Reset);
	process(clk_1Hz, Reset)
		variable tmp: integer range 0 to 64;
		begin
		if Reset = '1' then
			tmp := 0;
			LEDs <= (others => '0');
		elsif clk_1Hz'event AND clk_1Hz = '1' then
			LEDs(tmp) <= not LEDs(tmp);
			tmp := tmp+1;
			if tmp = 64 then 
				tmp := 0;
			end if;
		end if;	
	end process;
	update_matrix: entity work.Matrix port map(clk_10kHz, LEDs, Columns, Rows, Enable, Reset);
	-- RGB
	RGB_leds: entity work.RGB_animation port map(clk_1Hz, RGB1, RGB2, RGB3, Reset); 
	-- 7-segment displays
	f0hz1: entity work.freq_div generic map(240_000_000) port map(Clock, clk_0Hz1, Reset);
	process(clk_0Hz1, Reset)
		variable tmp: integer range 0 to 9999 := 0;
		begin
		if Reset = '1' then
			tmp := 0;
		elsif clk_0Hz1'event AND clk_0Hz1 = '1' then
			tmp := tmp+1;
			if tmp = 9999 then 
				tmp := 0;
			end if;
			Number <= tmp;
		end if;
	end process;	
	update_7_seg: entity work.Seven_seg port map();
end Behavioral;

