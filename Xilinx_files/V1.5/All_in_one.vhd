library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity All_in_one is
Port ( 
		Clock		 		: in	   STD_LOGIC;
		Reset		 		: in	   STD_LOGIC;
		-- Switches
		CLK				: out		STD_LOGIC := '0';
		Input_data 		: in   	STD_LOGIC;
		Parallel_load	: buffer STD_LOGIC := '0';
		-- Matrix
		Columns			: out		STD_LOGIC_VECTOR (2 downto 0);
		Rows 				: out		STD_LOGIC_VECTOR (2 downto 0);
		Enable 			: out		STD_LOGIC := '1';
		-- RGB 
		RGB1			 	: out		STD_LOGIC_VECTOR (2 downto 0);
		RGB2			 	: out		STD_LOGIC_VECTOR (2 downto 0);
		RGB3			 	: out		STD_LOGIC_VECTOR (2 downto 0);
		-- 7-seg
		Common_anodes 	: buffer	STD_LOGIC_VECTOR (3 downto 0);
		Segments 		: out  	STD_LOGIC_VECTOR (7 downto 0));
end All_in_one;

architecture Behavioral of All_in_one is
	signal clk_10kHz: 	STD_LOGIC;
	signal clk_1kHz: 	   STD_LOGIC;
	signal clk_1Hz: 		STD_LOGIC;
	signal clk_10Hz: 		STD_LOGIC;
	signal SW_data:		STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
	signal LEDs:			STD_LOGIC_VECTOR (0 to 63) :=(others => '0');
	signal Number:			integer range 0 to 9;
	begin
	-- switches
	f10khz: entity work.freq_div generic map(2_400) port map(Clock, clk_10kHz, Reset);
	CLK <= clk_10kHz;
	read_states_of_switches: entity work.SW_read port map(clk_10kHz, Input_data, Parallel_load, SW_data, Reset);
	-- matrix
	f1hz: entity work.freq_div generic map(24_000_000) port map(Clock, clk_1Hz, Reset);
--	process(clk_10Hz, Reset)
--		variable tmp: integer range 0 to 64;
--		begin
--		if Reset = '1' then
--			tmp := 0;
--			LEDs <= (others => '0');
--		elsif clk_10Hz'event AND clk_10Hz = '1' then
--			LEDs(tmp) <= not LEDs(tmp);
--			tmp := tmp+1;
--			if tmp >= 64 then 
--				tmp := 0;
--			end if;
--		end if;	
--	end process;
	LEDs(0 to 15) <= SW_data;
--	LEDs(0) <= SW_data(0);
--	LEDs(1) <= SW_data(1);
--	LEDs(2) <= SW_data(2);
--	LEDs(3) <= SW_data(3);
--	LEDs(4) <= SW_data(4);
--	LEDs(5) <= SW_data(5);
--	LEDs(6) <= SW_data(6);
--	LEDs(7) <= SW_data(7);
--	LEDs(8) <= SW_data(8);
--	LEDs(9) <= SW_data(9);
--	LEDs(10) <= SW_data(10);
--	LEDs(11) <= SW_data(11);
--	LEDs(12) <= SW_data(12);
--	LEDs(13) <= SW_data(13);
--	LEDs(14) <= SW_data(14);
--	LEDs(15) <= SW_data(15);
	update_matrix: entity work.Matrix port map(clk_10kHz, LEDs, Columns, Rows, Enable, Reset);
	-- RGB
	RGB_leds: entity work.RGB_animation port map(clk_1Hz, RGB1, RGB2, RGB3, Reset); 
	-- 7-segment displays
	f10hz: entity work.freq_div generic map(2_400_000) port map(Clock, clk_10Hz, Reset);
--	process(clk_10Hz, Reset)
--		variable tmp: integer range 0 to 10000 := 0;
--		begin
--		if Reset = '1' then
--			tmp := 0;
--		elsif clk_10Hz'event AND clk_10Hz = '1' then
--			tmp := tmp+1;
--			if tmp > 9999 then 
--				tmp := 0;
--			end if;
--		end if;
--		Number <= tmp;
--	end process;
	f1Khz: entity work.freq_div generic map(24_000) port map(Clock, clk_1KHz, Reset);	
process(clk_10Hz, Reset)
variable tmp: integer range 0 to 10 := 0;
begin
if clk_10Hz'event AND clk_10Hz = '1' then
			tmp := tmp+1;
			if tmp > 9 then 
				tmp := 0;
			end if;
end if;
Number <= tmp;
end process;

Common_anodes<="1111";
with Number select
	Segments<= "10000000" when 0,-- 0
		"01000000" when 1,-- 1
		"00100000" when 2,-- 2
		"00010000" when 3,-- 3
		"00001000" when 4,-- 4
		"00000100" when 5,-- 5
		"00000010" when 6,-- 6
		"00000001" when 7,-- 7
		"00000000" when 8,-- 8
		"11111111" when 9;-- 9
--	update_7_seg: entity work.Seven_seg port map(clk_1KHz, Number, Common_anodes, Segments, Reset);
end Behavioral;

