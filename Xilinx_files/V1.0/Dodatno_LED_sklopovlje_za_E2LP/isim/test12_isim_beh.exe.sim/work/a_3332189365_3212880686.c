/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/Xilinx_Projects/Dodatno_LED_sklopovlje_za_E2LP/SW_read.vhd";



static void work_a_3332189365_3212880686_p_0(char *t0)
{
    unsigned char t1;
    unsigned char t2;
    char *t3;
    unsigned char t4;
    char *t5;
    char *t6;
    unsigned char t7;
    unsigned char t8;
    char *t9;
    int t10;
    int t11;
    unsigned char t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    unsigned char t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    int t21;
    int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;

LAB0:    xsi_set_current_line(18, ng0);
    t3 = (t0 + 992U);
    t4 = xsi_signal_has_event(t3);
    if (t4 == 1)
        goto LAB8;

LAB9:    t2 = (unsigned char)0;

LAB10:    if (t2 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:    t3 = (t0 + 992U);
    t4 = xsi_signal_has_event(t3);
    if (t4 == 1)
        goto LAB16;

LAB17:    t2 = (unsigned char)0;

LAB18:    if (t2 == 1)
        goto LAB13;

LAB14:    t1 = (unsigned char)0;

LAB15:    if (t1 != 0)
        goto LAB11;

LAB12:
LAB3:    t3 = (t0 + 3272);
    *((int *)t3) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(19, ng0);
    t5 = (t0 + 3352);
    t13 = (t5 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    *((unsigned char *)t16) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t5);
    goto LAB3;

LAB5:    t5 = (t0 + 1968U);
    t9 = *((char **)t5);
    t10 = *((int *)t9);
    t11 = xsi_vhdl_mod(t10, 15);
    t12 = (t11 == 0);
    t1 = t12;
    goto LAB7;

LAB8:    t5 = (t0 + 1032U);
    t6 = *((char **)t5);
    t7 = *((unsigned char *)t6);
    t8 = (t7 == (unsigned char)2);
    t2 = t8;
    goto LAB10;

LAB11:    xsi_set_current_line(21, ng0);
    t5 = (t0 + 3352);
    t14 = (t5 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t20 = *((char **)t16);
    *((unsigned char *)t20) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t5);
    xsi_set_current_line(22, ng0);
    t3 = (t0 + 1968U);
    t5 = *((char **)t3);
    t10 = *((int *)t5);
    t11 = (t10 + 1);
    t3 = (t0 + 1968U);
    t6 = *((char **)t3);
    t3 = (t6 + 0);
    *((int *)t3) = t11;
    xsi_set_current_line(23, ng0);
    t3 = (t0 + 1968U);
    t5 = *((char **)t3);
    t10 = *((int *)t5);
    t1 = (t10 == 24);
    if (t1 != 0)
        goto LAB22;

LAB24:    t3 = (t0 + 1968U);
    t5 = *((char **)t3);
    t10 = *((int *)t5);
    t1 = (t10 > 7);
    if (t1 != 0)
        goto LAB25;

LAB26:
LAB23:    xsi_set_current_line(28, ng0);
    t3 = (t0 + 1968U);
    t5 = *((char **)t3);
    t10 = *((int *)t5);
    t3 = (t0 + 3480);
    t6 = (t3 + 56U);
    t9 = *((char **)t6);
    t13 = (t9 + 56U);
    t14 = *((char **)t13);
    *((int *)t14) = t10;
    xsi_driver_first_trans_fast(t3);
    goto LAB3;

LAB13:    t5 = (t0 + 1968U);
    t9 = *((char **)t5);
    t10 = *((int *)t9);
    t17 = (t10 > 0);
    if (t17 == 1)
        goto LAB19;

LAB20:    t5 = (t0 + 1352U);
    t13 = *((char **)t5);
    t18 = *((unsigned char *)t13);
    t19 = (t18 == (unsigned char)3);
    t12 = t19;

LAB21:    t1 = t12;
    goto LAB15;

LAB16:    t5 = (t0 + 1032U);
    t6 = *((char **)t5);
    t7 = *((unsigned char *)t6);
    t8 = (t7 == (unsigned char)3);
    t2 = t8;
    goto LAB18;

LAB19:    t12 = (unsigned char)1;
    goto LAB21;

LAB22:    xsi_set_current_line(24, ng0);
    t3 = (t0 + 1968U);
    t6 = *((char **)t3);
    t3 = (t6 + 0);
    *((int *)t3) = 0;
    goto LAB23;

LAB25:    xsi_set_current_line(26, ng0);
    t3 = (t0 + 1192U);
    t6 = *((char **)t3);
    t2 = *((unsigned char *)t6);
    t3 = (t0 + 1968U);
    t9 = *((char **)t3);
    t11 = *((int *)t9);
    t21 = (t11 - 8);
    t22 = (t21 - 0);
    t23 = (t22 * 1);
    t24 = (1 * t23);
    t25 = (0U + t24);
    t3 = (t0 + 3416);
    t13 = (t3 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    *((unsigned char *)t16) = t2;
    xsi_driver_first_trans_delta(t3, t25, 1, 0LL);
    goto LAB23;

}


extern void work_a_3332189365_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3332189365_3212880686_p_0};
	xsi_register_didat("work_a_3332189365_3212880686", "isim/test12_isim_beh.exe.sim/work/a_3332189365_3212880686.didat");
	xsi_register_executes(pe);
}
