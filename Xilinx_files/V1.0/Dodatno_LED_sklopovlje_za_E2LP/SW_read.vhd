library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SW_read is
	Port ( 
			Clock		 		: in   	STD_LOGIC;
			Clock_o	 		: out   	STD_LOGIC;
			Input_data 		: in   	STD_LOGIC;
         Parallel_load  : inout  STD_LOGIC := '0';
			SW_data			: out  	STD_LOGIC_VECTOR (15 downto 0):= (others => '0'));
end SW_read;

architecture Behavioral of SW_read is
	signal counter: integer range 0 to 16 := 0;
	begin
	Clock_o<=Clock;
	process(Clock)
		variable tmp: integer range 0 to 16 := counter;
		begin
		if(Clock'event AND Clock = '0' AND tmp = 0) then
			Parallel_load<='1';
		elsif(Clock'event AND Clock = '1' AND (tmp > 0 OR Parallel_load = '1')) then
			Parallel_load<='0';
			if(tmp = 16) then
				tmp:=0;
			else
				SW_data(15 - tmp)<= Input_data;
				tmp:=tmp+1;
			end if;
			counter<=tmp;
		end if;
		end process;
	end Behavioral;