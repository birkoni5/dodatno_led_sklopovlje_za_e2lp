
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity seven-seg is
    Port ( Clock 				: in  	STD_LOGIC;
           Number 			: in  	STD_LOGIC;
           Common_anodes 	: out  	STD_LOGIC_VECTOR (3 downto 0);
           Segments 			: out  	STD_LOGIC_VECTOR (7 downto 0);
			  Reset 				: out 	STD_LOGIC);
end seven-seg;

architecture Behavioral of seven-seg is

begin


end Behavioral;

