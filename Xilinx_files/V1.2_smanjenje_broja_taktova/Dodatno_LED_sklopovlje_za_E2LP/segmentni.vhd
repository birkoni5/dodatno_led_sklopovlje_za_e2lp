----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:42:51 03/02/2017 
-- Design Name: 
-- Module Name:    segmentni - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity segmentni is
    Port ( anoda : buffer  STD_LOGIC:='0';
           segmenti : out  STD_LOGIC_VECTOR (7 downto 0):="00000000";
           clk : in  STD_LOGIC);
end segmentni;

architecture Behavioral of segmentni is
signal clk_o: std_logic:='0';
type rgb_data is array (0 downto 2) of STD_LOGIC_VECTOR (2 downto 0);
signal RGB:			rgb_data:= ("000", "000","000");
begin

process(clk)
variable temp: integer range 0 to 2400000/2:=0;
begin
if (clk'event and clk='1') then
temp:=temp+1;
if (temp>=2400000/2) then
clk_o<=not clk_o;
temp:=0;
end if;
end if;
end process;
process(clk_o)
	begin
	if (clk_o'event and clk_o='1')then
		anoda<=not anoda;
		end if;
	end process;
end Behavioral;

