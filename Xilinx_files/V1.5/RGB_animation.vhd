library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RGB_animation is
    Port ( Clock 	: in  STD_LOGIC;
           L1 		: out  STD_LOGIC_VECTOR (2 downto 0);
           L2		: out  STD_LOGIC_VECTOR (2 downto 0);
           L3 		: out  STD_LOGIC_VECTOR (2 downto 0);
           Reset 	: in  STD_LOGIC);
end RGB_animation;

architecture Behavioral of RGB_animation is
	type state is (S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, S13, S14);
	signal sad_st, slj_st : state := S1;
	begin
	process(Clock, Reset)
		begin
		if Reset = '1' then
			sad_st <= S1;
		elsif (Clock'event and Clock='1') then
			sad_st<=slj_st;
		end if;
	end process;
	process(sad_st)
		begin
		case sad_st is
			when S1 => L1 <= "111"; L2 <= "111"; L3 <= "111"; slj_st<=S2;
			when S2 => L1 <= "011"; L2 <= "111"; L3 <= "111"; slj_st<=S3;
			when S3 => L1 <= "101"; L2 <= "111"; L3 <= "111"; slj_st<=S4;
			when S4 => L1 <= "110"; L2 <= "111"; L3 <= "111"; slj_st<=S5;
			when S5 => L1 <= "000"; L2 <= "011"; L3 <= "111"; slj_st<=S6;			
			when S6 => L1 <= "000"; L2 <= "101"; L3 <= "111"; slj_st<=S7;
			when S7 => L1 <= "000"; L2 <= "110"; L3 <= "111"; slj_st<=S8;
			when S8 => L1 <= "000"; L2 <= "000"; L3 <= "011"; slj_st<=S9;
			when S9 => L1 <= "000"; L2 <= "000"; L3 <= "101"; slj_st<=S10;
			when S10 => L1 <= "000"; L2 <= "000"; L3 <= "110"; slj_st<=S11;
			when S11 => L1 <= "011"; L2 <= "101"; L3 <= "110"; slj_st<=S12;
			when S12 => L1 <= "101"; L2 <= "110"; L3 <= "011"; slj_st<=S13;
			when S13 => L1 <= "110"; L2 <= "011"; L3 <= "101"; slj_st<=S14;
			when S14 => L1 <= "000"; L2 <= "000"; L3 <= "000"; slj_st<=S1;
		end case;
	end process;
end Behavioral;

