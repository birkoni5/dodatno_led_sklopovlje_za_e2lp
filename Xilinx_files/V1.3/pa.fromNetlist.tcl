
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name Test12 -dir "E:/VHDL/Test12/planAhead_run_3" -part xc6slx45fgg676-2
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/VHDL/Test12/All_in_one.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/VHDL/Test12} }
set_property target_constrs_file "UCF.ucf" [current_fileset -constrset]
add_files [list {UCF.ucf}] -fileset [get_property constrset [current_run]]
link_design
