LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY test12 IS
END test12;
 
ARCHITECTURE behavior OF test12 IS 
 
    COMPONENT SW_read
    PORT(
         Clock : IN  std_logic;
         Input_data : IN  std_logic;
         Parallel_load : INOUT  std_logic;
         SW_data : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal Clock : std_logic := '0';
   signal Input_data : std_logic := '0';

 	--Outputs
   signal Parallel_load : std_logic := '0';
   signal SW_data : std_logic_vector(15 downto 0):= "0000000000000000";

   -- Clock period definitions
   constant Clock_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: SW_read PORT MAP (
          Clock => Clock,
          Input_data => Input_data,
          Parallel_load => Parallel_load,
          SW_data => SW_data
        );

   -- Clock process definitions
   Clock_process :process
   begin
		Clock <= '0';
		wait for Clock_period/2;
		Clock <= '1';
		wait for Clock_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		Input_data<='1';
      wait for Clock_period*16;
		Input_data<='0';
      -- insert stimulus here 

      wait;
   end process;

END;
