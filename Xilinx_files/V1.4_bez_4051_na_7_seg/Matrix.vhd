library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL ;

entity Matrix is
    Port ( Clock 		: in     STD_LOGIC;
           LEDs 		: in     STD_LOGIC_VECTOR (0 to 63);
           Columns 	: out    STD_LOGIC_VECTOR (2 downto 0);
           Rows 		: out    STD_LOGIC_VECTOR (2 downto 0);
           Enable 	: out	   STD_LOGIC;
			  Reset		: in		STD_LOGIC);
end Matrix;

architecture Behavioral of Matrix is
	begin
	process(Clock, Reset)
		variable r: integer range 0 to 7;
		variable c: integer range 0 to 7;
		begin
		if Reset = '1' then
			r := 0;
			c := 0;
			Enable <= '1';
		elsif(Clock'event AND Clock = '1') then
			if(LEDs(r*8 + c) = '1') then
				Rows<=std_logic_vector( to_unsigned( r, 3));
				Columns<=std_logic_vector( to_unsigned( c, 3));
				Enable<='0';
			else 
				Enable <= '1';
			end if;
			c := c + 1;
			if c >= 8 then
				c := 0;
				r := r + 1;
			end if;
			if r >= 8 then
				r := 0;
			end if;
		end if;
	end process;
end Behavioral;