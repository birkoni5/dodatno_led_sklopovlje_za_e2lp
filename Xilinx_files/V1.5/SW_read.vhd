library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SW_read is
	Port ( 
			Clock		 		: in	   	STD_LOGIC;
			Input_data 		: in   		STD_LOGIC;
         Parallel_load  : buffer  	STD_LOGIC := '0';
			SW_data			: out  		STD_LOGIC_VECTOR (15 downto 0):= (others => '0');
			Reset		 		: in	   	STD_LOGIC);
end SW_read;

architecture Behavioral of SW_read is
	begin
	process(Clock, Reset)
		variable tmp: integer range 0 to 17 := 16;
		begin
		if Reset = '1' then
			tmp := 16;
			SW_data <= (others => '0');
			Parallel_load <= '0';
	   elsif rising_edge(Clock) then
			if tmp = 16 then
				Parallel_load <= '1';
				tmp := tmp+1;
			elsif tmp > 16 then
				Parallel_load <= '0';
				tmp := 0;
			else 
				SW_data(15 - tmp) <= Input_data;
				tmp := tmp+1;
			end if;
		end if;
	end process;
end Behavioral;