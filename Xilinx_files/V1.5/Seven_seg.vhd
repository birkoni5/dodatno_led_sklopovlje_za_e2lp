library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all; 
use ieee.std_logic_unsigned.all;

entity Seven_seg is
    Port ( Clock 				: in  		STD_LOGIC;
           Number 			: in  		INTEGER RANGE 0 TO 9999;
           Common_anodes 	: buffer  	STD_LOGIC_VECTOR (3 downto 0) := "0001";
           Segments 			: out  		STD_LOGIC_VECTOR (7 downto 0) := "00000000";
			  Reset 				: in	 		STD_LOGIC);
end Seven_seg;

architecture Behavioral of Seven_seg is
	type znamenke is array (3 downto 0) of integer range 0 to 9;
	signal num: znamenke := (0,0,0,0);
	signal tren_znam: integer range 0 to 3 := 0;
	begin
	process(Number)
		variable tmp_num: integer range 0 to 9999 := Number;
		begin
		num(3) <= tmp_num/1000;
		tmp_num := tmp_num - tmp_num/1000;
		num(2) <= tmp_num/100;
		tmp_num := tmp_num - tmp_num/100;
		num(1) <= tmp_num/10;
		tmp_num := tmp_num - tmp_num/10;
		num(0) <= tmp_num;
	end process;
	process(Clock, Reset)
    begin
        if Reset = '1' then
				Common_anodes <= "0001";
        elsif(Clock'event and Clock = '1') then
				case Common_anodes is
					when "0001" => Common_anodes <= "0010"; tren_znam <= 1;
					when "0010" => Common_anodes <= "0100"; tren_znam <= 2;
					when "0100" => Common_anodes <= "1000"; tren_znam <= 3;
					when "1000" => Common_anodes <= "0001"; tren_znam <= 0;
					when others => Common_anodes <= "0000";
				end case;
        end if;
    end process;
	 with num(tren_znam) select
			Segments  <= "11111100" when 0,  	  -- 0
							 "01100000" when 1,  	  -- 1
							 "11011010" when 2,  	  -- 2
							 "11110010" when 3,  	  -- 3
							 "01100110" when 4,  	  -- 4
							 "10110110" when 5,  	  -- 5
							 "10111110" when 6,    	  -- 6
							 "11100000" when 7,  	  -- 7
							 "11111110" when 8,  	  -- 8
							 "11110110" when others;  -- 9
end Behavioral;

