library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity freq_div is
	generic( nfCLK: natural :=100);
	port(
			clk  	: in 			std_logic:='0';
			clk_o	: buffer 	std_logic:='0';
			Reset	: in    		std_logic);
end freq_div;

architecture Behavioral of freq_div is
	begin	process(clk, Reset)
		variable temp: integer range 0 to nfCLK/2:=0;
		begin
		if Reset = '1' then
			temp := 0;
			clk_o <= '0';
		elsif (clk'event and clk='1') then
			temp := temp+1;
			if (temp >= nfCLK/2) then
				clk_o <= not clk_o;
				temp := 0;
			end if;
		end if;
	end process;
end Behavioral;