library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all; 
use ieee.std_logic_unsigned.all;

entity Seven_seg is
    Port ( Clock 				: in  		STD_LOGIC;
           Number 			: in  		INTEGER RANGE 0 TO 9999;
			  Enable_7 			: out  		STD_LOGIC;
           Common_anodes 	: buffer  	STD_LOGIC_VECTOR (3 downto 0) := "0001";
           Segments 			: out  		STD_LOGIC_VECTOR (7 downto 0) := "00000000";
			  Reset 				: in	 		STD_LOGIC);
end Seven_seg;

architecture Behavioral of Seven_seg is
	type znamenke is array (3 downto 0) of integer range 0 to 9;
	signal num: znamenke := (0,0,0,0);
	signal tren_znam: integer range 0 to 3 := 0;
	begin
	Enable_7 <= '0';
	process(Number)
		variable tmp_num: integer range 0 to 9999 := Number;
		begin
		num(3) <= tmp_num/1000;
		tmp_num := tmp_num - tmp_num/1000;
		num(2) <= tmp_num/100;
		tmp_num := tmp_num - tmp_num/100;
		num(1) <= tmp_num/10;
		tmp_num := tmp_num - tmp_num/10;
		num(0) <= tmp_num;
	end process;
	process(Clock, Reset)
    begin
        if Reset = '1' then
				Common_anodes <= "0001";
        elsif(Clock'event and Clock = '1') then
				for i in 0 to 3 loop
					if Common_anodes(i) = '1' and i = 0 then
						Common_anodes(3) <= '1';
						Common_anodes(i) <= '0';
						tren_znam <= 3;
					elsif Common_anodes(i) = '1' then
						Common_anodes(i-1) <= '1';
						Common_anodes(i) <= '0';
						tren_znam <= i-1;
					end if;
				end loop;
        end if;
    end process;
	 with num(tren_znam) select
			Segments  <= "00000011" when 0,  	  -- 0
							 "10011111" when 1,  	  -- 1
							 "00100101" when 2,  	  -- 2
							 "00001101" when 3,  	  -- 3
							 "10011001" when 4,  	  -- 4
							 "01001001" when 5,  	  -- 5
							 "01000001" when 6,    	  -- 6
							 "00011111" when 7,  	  -- 7
							 "00000001" when 8,  	  -- 8
							 "00001001" when others;  -- 9
end Behavioral;

