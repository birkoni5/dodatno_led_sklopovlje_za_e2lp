library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL ;

entity Matrix is
    Port ( Clock 		: in     STD_LOGIC;
           LEDs 		: in     STD_LOGIC_VECTOR (0 to 63);
           Columns 	: out    STD_LOGIC_VECTOR (2 downto 0);
           Rows 		: out    STD_LOGIC_VECTOR (2 downto 0);
           Enable 	: out	   STD_LOGIC);
end Matrix;

architecture Behavioral of Matrix is
	begin
	process(Clock)
		variable tmp: integer range 0 to 64 := 0;
		variable r: integer range 0 to 7 := tmp/8;
		variable c: integer range 0 to 7 := tmp mod 8;
		begin
		if(Clock'event AND Clock = '1') then
			if(LEDs(tmp) = '1') then
				Rows<=std_logic_vector( to_unsigned( r, 3));
				Columns<=std_logic_vector( to_unsigned( c, 3));
				Enable<='0';
			else 
				Enable <= '1';
			end if;
			tmp:=tmp+1;
			if(tmp >= 64) then
				tmp:=0;
			end if;
		end if;
	end process;
end Behavioral;